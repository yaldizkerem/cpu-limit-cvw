This repository contains a custom validation webhook. Checks the CPU limit for each container for pod creation and update requests. If more than "2" is given, it returns "admission.Denied" response.

## Quick Start

### Prerequisites

See [https://book.kubebuilder.io/quick-start.html#prerequisites](https://book.kubebuilder.io/quick-start.html#prerequisites) for prerequisites.

This repository depends on cert-manager, servicemonitors.monitoring.coreos.com CRD and prometheus.

See [https://helm.sh/docs/intro/install/](https://helm.sh/docs/intro/install/) for helm installation instructions.
```
# Deploy cert-manager chart
helm install cert-manager --namespace cert-manager --version v1.7.1 jetstack/cert-manager --create-namespace --set installCRDs=true

# Deploy prometheus operator
helm install prometheus-stack --namespace prometheus prometheus-community/kube-prometheus-stack --version 32.2.1 --create-namespace --set alertmanager.enabled=false --set grafana.enabled=false --set kubeApiServer.enabled=false --set kubelet.enabled=false --set kubeControllerManager.enabled=false --set coreDns.enabled=false --set kubeEtcd.enabled=false --set kubeScheduler.enabled=false --set kubeProxy.enabled=false --set defaultRules.create=false --set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false
```

### Development

```
# Spin up a kubernetes cluster with minikube
minikube start
```

```
# Configure your shell to use Docker daemon inside the minikube instance.
eval $(minikube docker-env)
```

```
# Build the project
make docker-build IMG=yaldizkerem/cpu-limit-validating-webhook:v1.1
```

```
# Deploy the project
make deploy IMG=yaldizkerem/cpu-limit-validating-webhook:v1.1
```

```
# Undeploy the project
make undeploy
```

```
# Build, undeploy and deploy the project
make docker-build undeploy deploy IMG=yaldizkerem/cpu-limit-validating-webhook:v1.1
```

```
# Push the image
make docker-push IMG=yaldizkerem/cpu-limit-validating-webhook:v1.1
```

```
# Generate the manifests
make manifests-gen IMG=yaldizkerem/cpu-limit-validating-webhook:v1.1
```

### Deployment

Deploy the project to a remote kubernetes cluster
```
# Build and push the image then deploy the project
make docker-build docker-push deploy IMG=yaldizkerem/cpu-limit-validating-webhook:v1.1
```
or
```
# Build and push the image then generate the manifests
make docker-build docker-push manifests-gen IMG=yaldizkerem/cpu-limit-validating-webhook:v1.1

# Apply the manifests file manually
kubectl -f apply artifacts/manifests.yaml
```

## Components

* [Kubebuilder](https://book.kubebuilder.io/introduction.html)

## Configuration

Update [config/manager/custom_validating_webhook_config.yaml](config/manager/custom_validating_webhook_config.yaml) to specify in which namespace the validation will be performed. Then deploy/redeploy the manifests.

Only default namespace
```
namespaces:
- default
```

All namespaces
```
ignorePerNamespaceConfigAndValidateAllNamespaces: true #if you omit this, the default value will be false
namespaces: []
```

default and platform namespace
```
ignorePerNamespaceConfigAndValidateAllNamespaces: false
namespaces:
- default
- platform
```

Disable validation
```
ignorePerNamespaceConfigAndValidateAllNamespaces: false
namespaces: []
```
