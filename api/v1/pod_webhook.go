/*
Copyright 2022 yaldizkerem.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"context"
	"fmt"
	"net/http"

	"gopkg.in/yaml.v2"
	corev1 "k8s.io/api/core/v1"
	"knative.dev/pkg/configmap"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// +kubebuilder:webhook:path=/validate-v1-pod,mutating=false,failurePolicy=fail,groups="",resources=pods,verbs=create;update,versions=v1,name=vpod.kb.io,admissionReviewVersions=v1,sideEffects=None

// contains
func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

type CustomValidatingWebhookConfig struct {
	IgnorePerNamespaceConfigAndValidateAllNamespaces bool     `yaml:"ignorePerNamespaceConfigAndValidateAllNamespaces"`
	Namespaces                                       []string `yaml:"namespaces"`
}

// podValidator validates Pods
type podValidator struct {
	Client                 client.Client
	decoder                *admission.Decoder
	namespacesConfig       string
	namespacesConfigMapKey string
}

func NewPodValidator(c client.Client, n string, k string) admission.Handler {
	return &podValidator{
		Client:                 c,
		namespacesConfig:       n,
		namespacesConfigMapKey: k,
	}
}

// podValidator denies a pod if cpu limit is above 2.
func (v *podValidator) Handle(ctx context.Context, req admission.Request) admission.Response {
	pod := &corev1.Pod{}

	err := v.decoder.Decode(req, pod)
	if err != nil {
		return admission.Errored(http.StatusBadRequest, err)
	}

	namespace := pod.Namespace

	cfg, _ := configmap.Load(v.namespacesConfig)

	var customValidatingWebhookConfig CustomValidatingWebhookConfig

	err = yaml.Unmarshal([]byte(cfg[v.namespacesConfigMapKey]), &customValidatingWebhookConfig)
	if err != nil {
		return admission.Errored(http.StatusBadRequest, err)
	}

	if customValidatingWebhookConfig.IgnorePerNamespaceConfigAndValidateAllNamespaces || contains(customValidatingWebhookConfig.Namespaces, namespace) {
		containers := pod.Spec.Containers
		for _, container := range containers {
			CPULimit, found := container.Resources.Limits.Cpu().AsInt64()
			if found && CPULimit > 2 {
				return admission.Denied(fmt.Sprintf("cpu limit is %d, must be less than or equal to 2", CPULimit))
			}
		}
	}

	return admission.Allowed("")
}

// podValidator implements admission.DecoderInjector.
// A decoder will be automatically injected.

// InjectDecoder injects the decoder.
func (v *podValidator) InjectDecoder(d *admission.Decoder) error {
	v.decoder = d
	return nil
}
